import tensorflow as tf
import tensorflow_probability as tfp

@tf.function
def compute_svgd_gradient(
    particles, # tensor
    score_func, # tf.function
    kernel # tfp.math.psd_kernel
):
    
    with tf.GradientTape(watch_accessed_variables=False) as kernel_tape:
        kernel_tape.watch(particles) # only watch the particles
        kernel_matrix = kernel.matrix(
            particles,
            tf.stop_gradient(particles) # stop gradient on the second input
        )
        kernel_all = tf.reduce_sum(kernel_matrix)

    with tf.GradientTape(watch_accessed_variables=False) as score_tape:
        score_tape.watch(particles)
        score_tensor = score_func(particles)
        score_all = tf.reduce_sum(score_tensor)

    repulsive_force = -kernel_tape.gradient(kernel_all, particles)
    score_gradient = score_tape.gradient(score_all, particles)

    score_gradient_no_nan =  tf.where(
        tf.math.is_nan(score_gradient), 
        tf.zeros_like(score_gradient), 
        score_gradient
    )

    phi = ( tf.tensordot( 
        tf.stop_gradient(kernel_matrix), # [N_samples x N_samples]
        score_gradient_no_nan, # [N_samples x features...]
        axes = [ [1], [0] ] # sum over the last for kernel_matrix, the first for score_gradient
    ) + repulsive_force ) / particles.shape[0]

    return phi, score_all, score_gradient, repulsive_force

if __name__ == "__main__":
    # TODO: add test scripts here
    pass
        

